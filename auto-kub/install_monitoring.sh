#!/usr/bin/bash

###############################################################
#  TITRE: add monitoring to the cluster
#
#  AUTEUR:   Xavier
#  VERSION: 
#  CREATION:  
#  MODIFIE: 
#
#  DESCRIPTION: 
###############################################################



# Variables ###################################################

IP_NFS=$(hostname -I | cut -d " " -f2)
echo $1
URL_PROMETHEUS=$1

# Functions ###################################################

install_kube_metrics(){

wget https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml -P /home/vagrant/monitoring/

sed -i s/"--kubelet-use-node-status-port"/"--kubelet-use-node-status-port\n        - --kubelet-insecure-tls"/g /home/vagrant/monitoring/components.yaml

kubectl apply -f /home/vagrant/monitoring/components.yaml

}

install_repos(){

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add kube-state-metrics https://kubernetes.github.io/kube-state-metrics
helm repo update

mkdir -p /home/vagrant/monitoring
chmod 775 -R /home/vagrant/monitoring
chown -R vagrant /home/vagrant/monitoring

}

install_monitoring_namespace(){

sudo -u vagrant bash -c "kubectl create ns monitoring"

}

install_persistent_volume_alertmanager(){

echo '
kind: StorageClass
apiVersion: storage.k8s.io/v1
metadata:
  name: alertmanager
  namespace: monitoring
provisioner: kubernetes.io/no-provisioner
volumeBindingMode: WaitForFirstConsumer
' > /home/vagrant/monitoring/sc-alertmanager.yml

kubectl apply -f /home/vagrant/monitoring/sc-alertmanager.yml

echo '
apiVersion: v1
kind: PersistentVolume
metadata:
  name: alertmanager
spec:
  storageClassName: alertmanager
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteMany
  persistentVolumeReclaimPolicy: Retain
  nfs:
    server: '${IP_NFS}'
    path: "/srv/monitoring/prometheus/"
' > /home/vagrant/monitoring/pv-alertmanager.yml

kubectl apply -f /home/vagrant/monitoring/pv-alertmanager.yml

}

install_persistent_volume_prometheus(){

echo '
kind: StorageClass
apiVersion: storage.k8s.io/v1
metadata:
  name: prometheus
  namespace: monitoring
provisioner: kubernetes.io/no-provisioner
volumeBindingMode: WaitForFirstConsumer
' > /home/vagrant/monitoring/sc-prometheus.yml

kubectl apply -f /home/vagrant/monitoring/sc-prometheus.yml

echo '
apiVersion: v1
kind: PersistentVolume
metadata:
  name: prometheus
spec:
  storageClassName: prometheus
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteMany
  persistentVolumeReclaimPolicy: Retain
  nfs:
    server: '${IP_NFS}'
    path: "/srv/monitoring/prometheus/"
' > /home/vagrant/monitoring/pv-prometheus.yml

kubectl apply -f /home/vagrant/monitoring/pv-prometheus.yml

}

install_helm_release_prometheus(){

echo '
---
apiVersion: helm.fluxcd.io/v1
kind: HelmRelease
metadata:
  name: prometheus
  namespace: monitoring
spec:
  releaseName: kube-prometheus-stack
  chart:
    name: kube-prometheus-stack
    version: 12.12.1
    repository: https://prometheus-community.github.io/helm-charts
  values:
    prometheus:
      enabled: true
      prometheusSpec:
        storageSpec:
          volumeClaimTemplate:
            spec:
              storageClassName: prometheus
              accessModes: ["ReadWriteMany"]
              resources:
                requests:
                  storage: 1Gi
' > /home/vagrant/monitoring/hr-prometheus.yml

kubectl apply -f /home/vagrant/monitoring/hr-prometheus.yml

}

install_service_ingress_prometheus(){

echo '
kind: Service
apiVersion: v1
metadata:
  name: prometheus
spec:
  type: ExternalName
  externalName: kube-prometheus-stack-prometheus.monitoring.svc.cluster.local
  ports:
  - port: 9090
' > /home/vagrant/monitoring/svc-prometheus.yml

kubectl apply -f /home/vagrant/monitoring/svc-prometheus.yml

echo '
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: prometheus
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - host: '${URL_PROMETHEUS}'
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: prometheus
            port:
              number: 9090
' > /home/vagrant/monitoring/ingress-prometheus.yml

kubectl apply -f /home/vagrant/monitoring/ingress-prometheus.yml

}

install_service_ingress_grafana(){

echo '
kind: Service
apiVersion: v1
metadata:
  name: grafana
spec:
  type: ExternalName
  externalName: kube-prometheus-stack-grafana.monitoring.svc.cluster.local
  ports:
  - port: 80
' > /home/vagrant/monitoring/svc-grafana.yml

kubectl apply -f /home/vagrant/monitoring/svc-grafana.yml

echo '
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: grafana
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - host: 'grafana.kub'
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: grafana
            port:
              number: 80
' > /home/vagrant/monitoring/ingress-grafana.yml

kubectl apply -f /home/vagrant/monitoring/ingress-grafana.yml

}

# Let's Go !! #################################################


install_repos
install_kube_metrics
install_monitoring_namespace
install_persistent_volume_alertmanager
install_persistent_volume_prometheus
install_helm_release_prometheus
install_service_ingress_prometheus
install_service_ingress_grafana
